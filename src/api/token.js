import axios from 'axios'
import { BASE_URL } from './base'

export const getToken = async () => await axios.get(`${BASE_URL}/token`)
