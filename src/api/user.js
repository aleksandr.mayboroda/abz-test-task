import axios from 'axios'
import { BASE_URL } from './base'

const config = {
  headers: {
    Token: localStorage.getItem('token'),
    'Content-Type': 'multipart/form-data',
  },
}

export const loadUsers = async (params) =>
  await axios.get(`${BASE_URL}/users`, { params })
  
export const registerUser = async (data) =>
  await axios.post(`${BASE_URL}/users`, data, config)
