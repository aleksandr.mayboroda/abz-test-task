import axios from 'axios'
import {BASE_URL} from './base'

export const loadPositions = async () => axios.get(`${BASE_URL}/positions`)