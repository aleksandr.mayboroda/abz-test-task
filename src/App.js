import React from 'react'
import { Box } from '@mui/system'

import MainPage from './pages/MainPage'

const App = () => {
  return (
    <Box sx={{bgcolor: 'background.paper', position: 'relative'}}>
      <MainPage />
    </Box>
  )
}

export default App
