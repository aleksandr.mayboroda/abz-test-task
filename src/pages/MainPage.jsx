import { useEffect } from 'react'
import { getToken } from '../api/token'

import Header from '../components/blocks/Header'
import TestAssignment from '../components/blocks/TestAssignment'
import UsersSection from '../components/blocks/UsersSection'
import FromSection from '../components/blocks/FromSection'

const MainPage = () => {
  const loadToken = async () => {
    try {
      const tokenResponse = await getToken()
      if (tokenResponse.status === 200 && tokenResponse.data.success === true) {
        localStorage.setItem('token', tokenResponse.data.token)
      }
      // console.log('token load', tokenResponse)
    } catch (err) {
      console.error('server error')
    }
  }

  useEffect(() => {
    loadToken()
  }, [])

  return (
    <>
      <Header />
      <TestAssignment />
      <UsersSection />
      <FromSection />
    </>
  )
}

export default MainPage
