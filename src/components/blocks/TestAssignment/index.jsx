import React from 'react'
import { Box, Button, Paper, Typography } from '@mui/material'
import ContainerOuter from '../../global/ContainerOuter'
import ContainerInner from '../../global/ContainerInner'

const TestAssignment = () => {
  return (
    <Paper
      sx={{
        background: `url(./image/testAssignment/background.png) no-repeat center`,
        padding: {
          sm: '40px 0 71px',
          md: '89px 0 88px',
          lg: '164px 0 163px',
        },
      }}
      square
    >
      <ContainerOuter>
        <ContainerInner>
          <Typography variant="h1" align="center" color="text.light">
            Test assignment for front-end developer
          </Typography>
          <Typography
            align="center"
            sx={{ mt: '21px', mb: '32px', color: 'text.light' }}
          >
            What defines a good front-end developer is one that has skilled
            knowledge of HTML, CSS, JS with a vast understanding of User design
            thinking as they'll be building web interfaces with accessibility in
            mind. They should also be excited to learn, as the world of
            Front-End Development keeps evolving.
          </Typography>
          <Box sx={{ display: 'flex', justifyContent: 'center' }}>
            <Button variant="contained">Sign up</Button>
          </Box>
        </ContainerInner>
      </ContainerOuter>
    </Paper>
  )
}

export default TestAssignment
