import { useEffect } from 'react'
import { Grid, Box, Button } from '@mui/material'

import { useSelector, useDispatch } from 'react-redux'
import { userOperations, userSelectors } from '../../../store/user'

import UserItem from '../../chunks/UserItem'
import Loader from '../../chunks/Loader'

const UserList = () => {
  const dispatch = useDispatch()
  const testDispatch = () => {
    dispatch(userOperations.getUsers())
  }

  const { list, isNextPage, isLoading } = useSelector(userSelectors.getData())

  const loadMore = async () => dispatch(userOperations.getMoreUsers())

  useEffect(() => {
    testDispatch()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const userList =
    !list.length > 0
      ? null
      : list.map((user) => <UserItem {...user} key={user.id} />)

  return (
    <>
      <Grid
        sx={{
          display: 'grid',
          gap: { xs: '16px', md: '29px' },
          gridTemplateColumns: {
            xs: 'repeat(1, 1fr)',
            md: 'repeat(2, 1fr)',
            lg: 'repeat(3, 1fr)',
          },
          mb: '50px',
        }}
      >
        {userList}
      </Grid>
      {isNextPage && (
        <Box sx={{ display: 'flex', justifyContent: 'center' }}>
          <Button variant="contained" onClick={loadMore}>
            Show More
          </Button>
        </Box>
      )}
      {isLoading && <Loader />}
    </>
  )
}

export default UserList
