import React from 'react'
import { Box, Button } from '@mui/material'
import Logo from '../../chunks/Logo'
import ContainerOuter from '../../global/ContainerOuter'

const Header = () => {
  return (
    <Box sx={{ bgcolor: 'background.light' }}>
      <ContainerOuter >
        <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
          <Logo />
          <Box sx={{ display: 'flex' }}>
            <Button variant="contained" sx={{ mr: '10px' }}>
              Users
            </Button>
            <Button variant="contained">Sign up</Button>
          </Box>
        </Box>
      </ContainerOuter>
    </Box>
  )
}

export default Header
