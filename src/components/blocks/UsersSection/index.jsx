import React from 'react'
import SectionContainer from '../../global/SectionContainer'
import UserList from '../UserList'

const UsersSection = () => {
  return (
      <SectionContainer titleText="Working with GET request">
        <UserList />
      </SectionContainer>
  )
}

export default UsersSection
