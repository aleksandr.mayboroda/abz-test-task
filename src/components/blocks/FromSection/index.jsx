import { useState, useEffect } from 'react'
import SectionContainer from '../../global/SectionContainer'
import ContainerInner from '../../global/ContainerInner'
import { useForm, FormProvider } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { useDispatch } from 'react-redux'
import { userOperations } from '../../../store/user'

import { loadPositions } from '../../../api/position'
import { registerUser } from '../../../api/user'

import registerSchema from '../../../assets/schemas/register'

import { Box, FormHelperText, Button, Typography } from '@mui/material'

import UploadField from '../../chunks/UploadField'
import CheckBoxGroup from '../../chunks/CheckBoxGroup'
import NumberInput from '../../chunks/NumberInput'
import CustomInput from '../../chunks/CustomInput'
import SuccessFormBlock from '../../chunks/SuccessFormBlock'
import ButtonContainer from './ButtonContainer'

const initialValues = {
  name: '',
  email: '',
  phone: '',
  position_id: 0,
  photo: [],
}

const FromSection = () => {
  const dispatch = useDispatch()

  const [positionList, setPositionList] = useState([])
  const [formIsSent, setFormIsSent] = useState(false) //to show image
  const [serverError, setServerError] = useState('')

  const methods = useForm({
    resolver: yupResolver(registerSchema),
    mode: 'onChange',
    defaultValues: initialValues,
  })

  const getPositions = async () => {
    setServerError('')
    try {
      const positionResult = await loadPositions()
      if (positionResult.status === 200) {
        setPositionList(positionResult.data.positions)
      }
    } catch (err) {
      console.error('server error')
      if (err.response.data.message) {
        setServerError(err.response.data.message)
      }
    }
  }

  const formSubmit = async (data) => {
    setServerError('')
    try {
      const newData = {
        ...data,
        photo: data.photo[0],
        // eslint-disable-next-line no-useless-escape
        phone: data.phone.replace(/[\s\(\)\-\_]/gi, ''), //make number +380xxXXXxxXX
      }

      const saveUserResponse = await registerUser(newData)
      if (saveUserResponse.status < 400) {
        setFormIsSent(true)
        dispatch(userOperations.returnToFirstPage())
        dispatch(userOperations.getUsers())
      }
    } catch (err) {
      console.error('server error')
      if (err.response.data.message) {
        setServerError(err.response.data.message)
      }
    }
  }

  useEffect(() => {
    getPositions()
  }, [])

  return (
    <SectionContainer
      titleText="Working with POST request"
      withTitle={!formIsSent}
    >
      <ContainerInner>
        {formIsSent && <SuccessFormBlock />}
        {!formIsSent && (
          <FormProvider {...methods}>
            <form onSubmit={methods.handleSubmit(formSubmit)}>
              <CustomInput name="name" label="Your name" type="text" />
              <CustomInput name="email" label="Email" type="email" />

              <NumberInput
                name="phone"
                label="Phone"
                mask={'_'}
                format={'+38 (###) ### - ## - ##'}
                helperText="+38 (0XX) XXX - XX - XX"
                control={methods.control}
              />

              <CheckBoxGroup
                control={methods.control}
                name="position_id"
                list={positionList}
              />

              <UploadField name="photo" label="Upload your photo" />

              <ButtonContainer>
                {serverError && (
                  <FormHelperText variant="filled" error sx={{ mb: '10px' }}>
                    {serverError}
                  </FormHelperText>
                )}
                <Button
                  variant="contained"
                  // disabled={!methods.isValid || methods.isSubmitting}
                  type="submit"
                >
                  Sign up
                </Button>
              </ButtonContainer>
            </form>
          </FormProvider>
        )}
        <Box>
          <Typography variant="h1" align="center" sx={{ mb: '50px' }} />
        </Box>
      </ContainerInner>
    </SectionContainer>
  )
}

export default FromSection
