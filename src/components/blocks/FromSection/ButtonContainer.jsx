import { Box, styled } from '@mui/material'

const ButtonContainer = styled(Box)(() => ({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  flexDirection: 'column',
  marginTop: '50px',
  paddingBottom: '100px',
}))

export default ButtonContainer
