import React from 'react'
import { Box } from '@mui/material'

const ContainerInner = ({ children }) => {
  return (
    <Box
      sx={{
        maxWidth: {
          xs: '328px',
          md: '380px',
        },
        margin: 'auto',
      }}
    >
      {children}
    </Box>
  )
}

export default ContainerInner
