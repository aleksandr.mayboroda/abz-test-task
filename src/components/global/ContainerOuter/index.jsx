import React from 'react'

import { Container } from '@mui/material'

const ContainerOuter = ({ children, ...props }) => {
  return (
    <Container
      sx={{
        padding: {
          xs: '13px 16px',
          md: '13px 32px',
          lg: '13px 60px',
        },
      }}
      {...props}
    >
      {children}
    </Container>
  )
}

export default ContainerOuter
