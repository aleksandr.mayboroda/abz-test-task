import React from 'react'
import { Typography } from '@mui/material'
import ContainerOuter from '../ContainerOuter'

const SectionContainer = ({ children, titleText, withTitle = true }) => {
  return (
    <ContainerOuter sx={{ pt: '140px' }}>
      {withTitle && (
        <Typography variant="h1" align="center" sx={{ mb: '50px' }}>
          {titleText}
        </Typography>
      )}
      {children}
    </ContainerOuter>
  )
}

export default SectionContainer
