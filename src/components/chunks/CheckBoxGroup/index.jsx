import React from 'react'
import { useController } from 'react-hook-form'

import {
  Box,
  FormHelperText,
  FormControlLabel,
  Radio,
  Typography,
  RadioGroup,
} from '@mui/material'

const CheckBoxGroup = ({ name, control, list }) => {
  const {
    field: { onChange },
    fieldState: { error },
  } = useController({ name, control })

  const change = (ev) => {
    onChange(ev.target.value)
  }

  const radioList =
    !list?.length > 0
      ? null
      : list.map(({ id, name }) => (
          <FormControlLabel
            key={id}
            value={id}
            control={<Radio color="radio" />}
            label={name}
          />
        ))

  return (
    <Box sx={{ mb: '50px' }}>
      <Typography variant="p">{'Select your position'}</Typography>
      <RadioGroup aria-labelledby={'position_id'} onChange={change}>
        {radioList}
      </RadioGroup>
      {error && (
        <FormHelperText variant="filled" error>
          {error.message}
        </FormHelperText>
      )}
    </Box>
  )
}

export default CheckBoxGroup
