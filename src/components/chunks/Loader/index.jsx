import React from 'react'
import { Box, CircularProgress, styled } from '@mui/material'

const StyledBlock = styled(Box)(({ theme, invalid }) => ({
  position: 'fixed',
  backgroundColor: theme.palette.background.light,
  top: 0,
  left: 0,
  right: 0,
  bottom: 0,
  opacity: .9,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}))

const Loader = () => {
  return (
    <StyledBlock>
      <CircularProgress color={'radio'} size={60}/>
    </StyledBlock>
  )
}

export default Loader
