import React from 'react'
import { useFormContext } from 'react-hook-form'
import { Box, TextField, FormHelperText } from '@mui/material'

const CustomInput = ({
  name,
  label,
  type = 'text',
  helperText,
}) => {
  const {
    register,
    formState: { errors },
  } = useFormContext()

  const isError = !!errors[name]

  return (
    <Box sx={{ mb: '50px' }}>
      <TextField
        name={name}
        label={label}
        type={type}
        variant="outlined"
        color="border"
        error={isError}
        fullWidth
        {...register(name)}
      />
      {!isError && helperText && (
        <FormHelperText variant="filled" type="inputHelper">
          {helperText}
        </FormHelperText>
      )}
      {isError && (
        <FormHelperText variant="filled" error>
          {errors[name].message}
        </FormHelperText>
      )}
    </Box>
  )
}

export default CustomInput
