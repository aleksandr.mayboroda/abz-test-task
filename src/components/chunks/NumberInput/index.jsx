import React from 'react'
import { useController } from 'react-hook-form'

import { Box, TextField, FormHelperText } from '@mui/material'

import NumberFormat from 'react-number-format'

const NumberInput = ({ name, helperText, control, ...rest }) => {
  const {
    field: {onChange},
    fieldState: { error },
  } = useController({ name, control })

  const change = (ev) => {
    onChange(ev.formattedValue)
  }

  return (
    <Box sx={{ mb: '50px' }}>
      <NumberFormat
        variant="outlined"
        color="border"
        error={!!error}
        fullWidth
        customInput={TextField}
        {...rest}
        onValueChange={change}
      />
      {!error && helperText && (
        <FormHelperText variant="filled" type="inputHelper">
          {helperText}
        </FormHelperText>
      )}
      {error && (
        <FormHelperText variant="filled" error>
          {error.message}
        </FormHelperText>
      )}
    </Box>
  )
}

export default NumberInput
