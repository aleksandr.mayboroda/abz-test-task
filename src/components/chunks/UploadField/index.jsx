import React from 'react'
import { useFormContext } from 'react-hook-form'
import { FormHelperText } from '@mui/material'

import Label from './Label'
import Input from './Input'
import UploadBtn from './UploadBtn'
import StyledTypography from './StyledTypography'

const UploadField = ({ name, label }) => {
  const {
    register,
    getValues,
    formState: { errors },
  } = useFormContext()
  const isError = !!errors[name]

  const fileName = getValues(name)?.length > 0 ? getValues(name)[0].name : ''

  return (
    <>
      <Label htmlFor={name}>
        <Input id={name} name={name} type="file" {...register(name)} />
        <UploadBtn
          variant="outlined"
          component="span"
          color={'button'}
          invalid={`${isError ? '1' : ''}`}
        >
          Upload
        </UploadBtn>
        <StyledTypography invalid={`${isError ? '1' : ''}`} variant="p">
          {fileName ? fileName : label}
        </StyledTypography>
      </Label>
      {isError && (
        <FormHelperText variant="filled" error>
          {errors[name].message}
        </FormHelperText>
      )}
    </>
  )
}

export default UploadField
