import { Typography, styled } from '@mui/material'

const StyledTypography = styled(Typography)(({ theme, invalid }) => ({
  border: `1px solid ${
    Boolean(invalid) ? theme.palette.error.main : theme.palette.border.main
  }`,
  borderLeft: Boolean(invalid)
    ? 'none'
    : `1px solid ${theme.palette.border.main}`,
  color: theme.palette.text.secondary,
  padding: '14px 15px 16px 14px',
  lineHeight: '26px',
  flexGrow: 1,
  display: 'flex',
  alignItems: 'center',
  borderRadius: `0 ${theme.shape.borderRadiusSecondary} ${theme.shape.borderRadiusSecondary} 0`,
  whiteSpace: 'nowrap',
  overflow: 'hidden',
  textOverflow: 'ellipsis',
}))

export default StyledTypography
