import { styled } from '@mui/material'

const Input = styled('input')({
  display: 'none',
})
export default Input