import { Button, styled } from '@mui/material'

const UploadBtn = styled(Button)(({ theme, invalid }) => ({
  border: `1px solid ${
    invalid ? theme.palette.error.main : theme.palette.text.primary
  }`,
  color: theme.palette.text.primary,
  padding: '14px 15px',
  borderRadius: `${theme.shape.borderRadiusSecondary} 0 0 ${theme.shape.borderRadiusSecondary}`,
}))

export default UploadBtn
