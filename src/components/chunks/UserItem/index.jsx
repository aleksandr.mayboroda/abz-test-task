import React from 'react'
import { Grid, Typography, Box, Paper, Tooltip } from '@mui/material'
import { toShortString } from '../../../assets/helpers/strings'

const UserItem = ({ name, photo, position, email, phone }) => {
  return (
    <Grid
      component={Paper}
      container
      sx={{
        backgroundColor: 'background.light',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: '20px',
      }}
      elevation={0}
    >
      <Box
        sx={{
          borderRadius: '50px',
          overflow: 'hidden',
          width: '70px',
          height: '70px',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <img src={photo} alt={name} />
      </Box>
      <Typography sx={{ mt: '20px', mb: '20px' }}>
        {toShortString(name, 20)}
      </Typography>
      <Box>
        <Typography align="center">{position}</Typography>
        <Tooltip title={email}>
          <Typography align="center" sx={{ cursor: 'pointer' }}>
            {toShortString(email, 20)}
          </Typography>
        </Tooltip>
        <Typography align="center">{toShortString(phone, 20)}</Typography>
      </Box>
     
    </Grid>
  )
}

export default UserItem
