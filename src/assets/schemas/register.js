import * as yup from 'yup'

const isRequiredError = 'this field is required'

const allowedImages = ['image/jpeg', 'image/jpg ']
const MAX_FILE_SIZE = 5000000 //mb

const signUpSchema = yup
  .object({
    name: yup
      .string()
      .min(2, 'need 2 chars min')
      .max(60, 'need 60 chars max')
      .required(isRequiredError),
    email: yup
      .string()
      .email()
      .min(2, 'need 2 chars min')
      .max(100, 'need 100 chars max')
      .required(isRequiredError),
    phone: yup
      .string()
      .matches(
        /\+38\s\(0[\d]{2}\)\s[\d]{3}\s-\s[\d]{2}\s-\s[\d]{2}/,
        'Phone need to be in format +38(0xx) XXX-XX-XX'
      )
      .required(isRequiredError),
    position_id: yup
      .number(isRequiredError)
      .integer(isRequiredError)
      .positive(isRequiredError)
      .required(isRequiredError),
    photo: yup
      .mixed()
      .test({
        message: 'Please provide a supported file type: .jpg, .jpeg',
        test: (file, context) => {
          let isValid = false
          // console.log(file, isValid)
          if (!file || file.length < 1) {
            return isValid
          }

          isValid = allowedImages.includes(file[0].type)

          return isValid
        },
      })
      .test({
        message: `File too big, can't exceed ${MAX_FILE_SIZE / 1000000} mb`,
        test: (file, context) => {
          let isValid = false
          if (file && file[0] && file[0].size <= MAX_FILE_SIZE) {
            isValid = true
          }
          return isValid
        },
      }),
  })
  .required()

export default signUpSchema
