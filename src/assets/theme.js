import { createTheme } from '@mui/material/styles'

const theme = createTheme({
  breakpoints: {
    keys: ['xs', 'md', 'lg', 'xl'],
    values: {
      xs: 230,
      sm: 360,
      md: 768,
      lg: 1024,
      xl: 1170,
    },
  },
  palette: {
    primary: {
      light: '#F4E041',
      main: '#F4E041',
      dark: '#FFE302',
    },
    radio: {
      main: '#00BDD3',
    },
    background: {
      light: '#fff',
      paper: '#f8f8f8',
    },
    border: { main: '#D0CFCF' },
    button: {
      main: '#000',
    },
    error: { main: '#CB3D40' },
    text: {
      primary: 'rgba(0, 0, 0, 0.87)',
      secondary: '#7E7E7E',
      disabled: '#fff',
      light: '#fff',
    },
    action: {
      disabled: '#fff', //color
      disabledBackground: '#B4B4B4', //bg
    },
  },
  shape: {
    borderRadius: 10,
    borderRadiusSecondary: '4px',
  },
  typography: {
    fontFamily: 'Nunito, sans-serif',
    htmlFontSize: 16,
    fontSize: 16,
    lineHeight: '26px',
    fontWeight: 400,
    fontWeightRegular: 400,
    h1: {
      fontSize: '40px',
      lineHeight: '40px',
    },
    p: {
      fontSize: 16,
      lineHeight: '26px',
    },
    helperText: {
      fontSize: 12,
      lineHeight: '14px',
    },
  },

  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          fontSize: 16,
          lineHeight: '26px',
          textTransform: 'capitalize',
        },
      },
      variants: [
        {
          props: { variant: 'contained' },
          style: {
            borderRadius: '80px',
            minWidth: '100px',
          },
        },
      ],
    },
    MuiInputBase: {
      styleOverrides: {
        input: {
          fontSize: 16,
          lineHeight: '26px',
        },
        root: {
          [`& fieldset`]: {
            borderRadius: 4,
          },
        },
        [`&:hover`]: {
          borderColor: 'transparent',
        },
      },
    },
    MuiRadio: {
      styleOverrides: {
        root: {
          color: '#D0CFCF', //border grey when unchecked
        },
      },
    },
    MuiFormControlLabel: {
      styleOverrides: {
        label: {
          fontSize: 16,
          lineHeight: '26px',
        },
      },
    },
    MuiFormHelperText: {
      variants: [
        {
          props: { variant: 'filled', type: 'inputHelper' },
          style: {
            fontSize: 12,
            lineHeight: '14px',
            marginLeft: 16,
          },
        },
      ],
    },
    MuiTooltip: {
      styleOverrides: {
        tooltip: {
          borderRadius: '4px',
        },
      },
    },
  },
})

export default theme
