//to short strings to max
export const toShortString = (str, maxLength) =>
  str.length >= maxLength ? str.substring(0, +maxLength) + '...' : str
