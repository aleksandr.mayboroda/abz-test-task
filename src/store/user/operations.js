import actions from './actions'
import { loadUsers } from '../../api/user'

const { setCurrentPage, setPerPage } = actions

const getUsers = () => async (dispatch, getState) => {
  dispatch(actions.setIsLoading(true))
  try {
    const page = getState().user.currentPage
    const count = getState().user.perPage
    const offset = page > 1 ? (page - 1) * count : 0
    const usersResponse = await loadUsers({
      page,
      count,
      offset,
    })
    if (usersResponse.status === 200) {
      dispatch(actions.setUserList(usersResponse.data.users))
      const isNextPage = usersResponse.data.total_pages > page ? true : false
      dispatch(actions.setIsNextPage(isNextPage))
    }
    dispatch(actions.setIsLoading(false))
  } catch (err) {
    console.error('server error')
    dispatch(actions.setIsLoading(false))
  }
}

const getMoreUsers = () => async (dispatch, getState) => {
  try {
    let page = getState().user.currentPage + 1 //set + 1 page here

    dispatch(actions.setCurrentPage(page)) 
    dispatch(actions.setIsLoading(true))

    const count = getState().user.perPage
    const offset = page > 1 ? (page - 1) * count : 0
    const usersResponse = await loadUsers({
      page: page,
      count,
      offset,
    })
    if (usersResponse.status === 200) {
      dispatch(actions.addUsersToList(usersResponse.data.users))
      const isNextPage = usersResponse.data.total_pages > page ? true : false
      dispatch(actions.setIsNextPage(isNextPage))
    }
    dispatch(actions.setIsLoading(false))
  } catch (err) {
    console.error('server error')
    dispatch(actions.setIsLoading(false))
  }
}

const returnToFirstPage = () => async (dispatch) => {
  dispatch(actions.setCurrentPage(1))
}

const def = {
  setCurrentPage,
  setPerPage,
  getUsers,
  getMoreUsers,
  returnToFirstPage,
}

export default def
