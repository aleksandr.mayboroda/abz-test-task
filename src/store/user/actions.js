import types from './types'

const setUserList = (list) => ({
  type: types.SET_USER_LIST,
  payload: list,
})

const addUsersToList = (users) => ({
  type: types.ADD_USERS_TO_LIST,
  payload: users,
})

const setCurrentPage = (page) => ({
  type: types.SET_CURRENT_PAGE,
  payload: page,
})

const setPerPage = (perPage) => ({
  type: types.SET_PER_PAGE,
  payload: perPage,
})

const setIsLoading = (val) => ({
  type: types.SET_IS_LOADING,
  payload: val,
})

const setIsNextPage = (val) => ({
  type: types.SET_IS_NEXT_PAGE,
  payload: val,
})

const def = {
  setUserList,
  addUsersToList,
  setCurrentPage,
  setPerPage,
  setIsLoading,
  setIsNextPage,
}

export default def
