const SET_USER_LIST = 'abz/user/SET_USER_LIST'
const ADD_USERS_TO_LIST = 'abz/user/ADD_USERS_TO_LIST'
const SET_CURRENT_PAGE = 'abz/user/SET_CURRENT_PAGE'
const SET_PER_PAGE = 'abz/user/SET_PER_PAGE'
const SET_IS_LOADING = 'abz/user/SET_IS_LOADING'
const SET_IS_NEXT_PAGE = 'abz/user/SET_IS_NEXT_PAGE'

const def = {
  SET_USER_LIST,
  ADD_USERS_TO_LIST,
  SET_CURRENT_PAGE,
  SET_PER_PAGE,
  SET_IS_LOADING,
  SET_IS_NEXT_PAGE,
}

export default def
