const getData = () => state => state.user
const getList = () => state => state.user.list

const def = {
  getData, getList
}

export default def