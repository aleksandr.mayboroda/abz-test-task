import types from './types'

const initialValue = {
  list: [],
  currentPage: 1,
  perPage: 6,
  isLoading: false,
  isNextPage: false
}

const reducer = (state = initialValue, action) => {
  switch (action.type) {
    case types.SET_CURRENT_PAGE: {
      return {...state, currentPage: action.payload}
    }
    case types.SET_PER_PAGE: {
      return {...state, perPage: action.payload}
    }
    case types.SET_IS_LOADING: {
      return {...state, isLoading: action.payload}
    }
    case types.SET_IS_NEXT_PAGE: {
      return {...state, isNextPage: action.payload}
    }
    case types.SET_USER_LIST: {
      return {...state, list: action.payload}
    }
    case types.ADD_USERS_TO_LIST: {
      const newList = [...state.list, ...action.payload]
      const newState = {...state, list: newList} 
      console.log('redux 222',newState)
      return {...state, list: newList} 
    }
    default:
      return state
  }
}

export default reducer
