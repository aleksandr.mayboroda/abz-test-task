ABZ Test Task
figma: https://www.figma.com/file/ykJhQGVFGbQBEQZzuktwvm/TESTTASK---2022?node-id=581%3A0
      https://www.figma.com/proto/ykJhQGVFGbQBEQZzuktwvm/TESTTASK---2022?page-id=581%3A0&node-id=3373%3A30897&viewport=358%2C48%2C0.04&scaling=min-zoom&starting-point-node-id=3373%3A30897&show-proto-sidebar=1

TT: https://drive.google.com/file/d/11XPGP9wrHB-B5aLlBCRLXHVA2yvM2QhY/view

API docs: https://apidocs.abz.dev/test_assignment_for_frontend_developer_api_documentation#users_post


Used Technologies:
- react 18v
- redux, thunk
- material ui 5
- react-hook-form


Description:
Users: on page load automaticly get 6 users. If there any more pages, button 'show more' is shown. By pressing the button, a request is made to the server and new users of 6 people are inserted at the end of the list.

Form: validation with yup, after submit form disappears and list of users reloads to last 6.